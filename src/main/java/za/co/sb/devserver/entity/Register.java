package za.co.sb.devserver.entity;

import java.io.Serializable;
import java.net.URL;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQuery;
import javax.validation.constraints.Email;
import javax.validation.constraints.NotNull;

/**
 *
 * @author steven
 */
@Entity
@NamedQuery(name="Register.findByServiceName", query="SELECT r from Register r where r.serviceName = :serviceName")
@NamedQuery(name="Register.findAll", query="SELECT r from Register r")
@NamedQuery(name="Register.deleteAllByName", query="SELECT r from Register r where r.serviceName = :serviceName")
@NamedQuery(name="Register.deleteAll", query="DELETE from Register")
public class Register implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;
    
    @NotNull
    @Column(unique = true)
    private String serviceName;
    
    @Email(message = "Email address is invalid")
    private String email;
    
    @NotNull
    private URL registrationUrl;
    
    @NotNull        
    private URL andonCordStateChangeUrl;
    
    @NotNull
    private String location;
    
    private URL metricsUrl;
    
    @NotNull
    private URL healthCheckUrl;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getServiceName() {
        return serviceName;
    }

    public void setServiceName(String serviceName) {
        this.serviceName = serviceName;
    }    

    public String getLocation() {
        return location;
    }

    public void setLocation(String location) {
        this.location = location;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public URL getRegistrationUrl() {
        return registrationUrl;
    }

    public void setRegistrationUrl(URL registrationUrl) {
        this.registrationUrl = registrationUrl;
    }

    public URL getAndonCordStateChangeUrl() {
        return andonCordStateChangeUrl;
    }

    public void setAndonCordStateChangeUrl(URL andonCordStateChangeUrl) {
        this.andonCordStateChangeUrl = andonCordStateChangeUrl;
    }

    public URL getMetricsUrl() {
        return metricsUrl;
    }

    public void setMetricsUrl(URL metricsUrl) {
        this.metricsUrl = metricsUrl;
    }

    public URL getHealthCheckUrl() {
        return healthCheckUrl;
    }

    public void setHealthCheckUrl(URL healthCheckUrl) {
        this.healthCheckUrl = healthCheckUrl;
    }

    
    
    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {        
        if (!(object instanceof Register)) {
            return false;
        }
        Register other = (Register) object;
        return !((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id)));
    }

    @Override
    public String toString() {
        return "za.co.sb.devserver.entity.Register[ id=" + id + " ]";
    }
    
}
