/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package za.co.sb.devserver.health;

import javax.enterprise.context.ApplicationScoped;
import org.eclipse.microprofile.health.Health;
import org.eclipse.microprofile.health.HealthCheck;
import org.eclipse.microprofile.health.HealthCheckResponse;

/**
 *
 * @author steven
 */
@Health
@ApplicationScoped
public class MemoryHealth implements HealthCheck {

    private Runtime runtime = Runtime.getRuntime();

    @Override
    public HealthCheckResponse call() {
        long freeMemory = runtime.freeMemory();
        long maxMemory = runtime.totalMemory();
        long tenPercentOfMax = (maxMemory / 100) * 10;
        double percentageFree = ((double)freeMemory/maxMemory)  * 100;
        if (freeMemory < tenPercentOfMax) {
            return HealthCheckResponse.named("memory-check")
                    .withData("Free Memory", freeMemory / 1024)
                    .withData("Max Memory", maxMemory / 1024)
                    .withData("Percentage Free", percentageFree + "%")
                    .down()
                    .build();
        } else {
            return HealthCheckResponse.named("memory-check")
                    .withData("Free Memory", freeMemory / 1024)
                    .withData("Max Memory", maxMemory / 1024)
                    .withData("Percentage Free", percentageFree + "%")
                    .up()
                    .build();
        }
    }
}
